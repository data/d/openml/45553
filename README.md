# OpenML dataset: FICO-HELOC-cleaned

https://www.openml.org/d/45553

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

This dataset is from the "Explainable Machine Learning Challenge":

> The Explainable Machine Learning Challenge is a collaboration between Google, FICO and academics at Berkeley, Oxford, Imperial, UC Irvine and MIT, to generate new research in the area of algorithmic explainability. Teams will be challenged to create machine learning models with both high accuracy and explainability; they will use a real-world financial dataset provided by FICO. Designers and end users of machine learning algorithms will both benefit from more interpretable and explainable algorithms. Machine learning model designers will benefit from Model explanations, written explanations describing the functioning of a trained model. These might include information about which variables or examples are particularly important, they might explain the logic used by an algorithm, and/or characterize input/output relationships between variables and predictions. We expect teams to tell the story of their model such that these explanations will be qualitatively evaluated by data scientists at FICO.

Further information can be retrieved from the [FICO website](https://community.fico.com/s/explainable-machine-learning-challenge).

**Notes**
* We have obtained the dataset from [Kaggle](https://www.kaggle.com/datasets/averkiyoliabev/home-equity-line-of-creditheloc)
* This is a cleaned version of the Kaggle dataset, in which we have removed all rows that only contained `-9`, a special value according to the FAQ.
* Please request access to the data on the FICO website to obtain the full description of the features.
* In this version we have encoded the special values (-9, -8, -7) as missing values to make the data more amenable to non-tree models.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/45553) of an [OpenML dataset](https://www.openml.org/d/45553). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/45553/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/45553/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/45553/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

